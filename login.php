<?php  
/** 
 * Template Name: login page 
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> style="height: 100vh;">

    <div class="container login-container">

    <style>
        .page-id-0 {
            height: 100vh;
        }
        .login-container {
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        @media (min-width: 992px) {
            .login-container {
                justify-content: start;
            }
        }
        .login-container form {
            width: 20rem;
        }
        .login-username,
        .login-password {
            display: flex;
            justify-content: space-between;
        }
        input {
            width: 200px;
            border: 1px solid lightgray;
            border-radius: 0.1875rem;
        }
        .login-remember,
        .login-submit {
            display: flex;
            justify-content: start;
        }
        .login-remember label {
            display: flex;
            justify-content: start;
            align-items: center;
        }
        .login-remember input[type="checkbox"] {
            width: 1rem;
            margin-right: 0.625rem;
        }
        .login-submit {
            display: flex;
            align-items: center;
            line-height: 1rem;
            height: auto;
        }
        input#wp-submit {
            height: auto;
            padding: 1rem;
            border: 1px solid #d17f48;
            background: #fbf2ec;
            color: #364a57;
            border-radius: 0.1875rem;
            transition: 300ms;
        }
        input#wp-submit:hover {
            height: auto;
            padding: 1rem;
            border: 1px solid #d17f48;
            background: #d17f48;
            color: #fbf2ec;
            border-radius: 0.1875rem;
        }
    </style>

    <?php

        if ( ! is_user_logged_in() ) { // Display WordPress login form:
            $args = array(
                'redirect' => admin_url(),
                // 'form_id' => 'proof-loginform',
                'label_username' => __( 'Username' ),
                'label_password' => __( 'Password' ),
                'label_remember' => __( 'Remember Me' ),
                'label_log_in' => __( 'Log In' ),
                'remember' => true,
                'echo' => true,
            );

            wp_login_form( $args );

        } else { // If logged in:
            wp_loginout( home_url() ); // Display "Log Out" link.
            echo " ";
        }

        echo '
                    </div>

                ' . wp_footer() . '
                </body>
            </html>
        ';

        
    ?> 