<?php
/**
 * The searchform.php template.
 *
 * @package Proof Drinks
 */

echo '
    <form class="header-search" role="search" action="' . esc_url( home_url( '/' ) ) . '">
        <button type="submit" class="header-search-button">
            <i class="fas fa-search"></i>
        </button>
        <input id="search-term" class="header-search-bar" type="text" placeholder="Quick Search..." autocomplete="off" value="' . get_search_query() . '" name="s">
        <input type="hidden" value="product" name="post_type">
    </form>
';
        

// <style>
//     .header-search {
//         display: flex;
//         position: fixed;
//         z-index: 9999;
//         top: 133px;
//         left: 7px;
//     }
//     @media (min-width: 768px) {
//         .header-search {
//             top: 124px;
//             left: 40px;
//         }
//     }
//     .header-search-button {
//         border: none;
//         color: #364a57;
//         background: none;
//     }
// </style>