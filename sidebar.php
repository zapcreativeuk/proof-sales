<?php 
    $proofSidebar = 'proof_sidebar_menu';
    
    echo '
        <aside id="sidebar" class="sidebar col-12 col-md-2 d-flex flex-column align-items-center order-2 order-lg-1">
    ';

    if( is_active_sidebar( $proofSidebar ) ) {
        echo '
            <ul id="sidebar">' . dynamic_sidebar( $proofSidebar ) . '</ul>
        ';
    };

    echo '
        </aside>
    ';
?>
