<?php
/**
 * Index page file for the Proof Sales theme.
 */

    get_header();

        echo '<main id="main">';
        
            while( have_posts() ) :
                the_post();    
                the_content();                    
            endwhile;
            wp_reset_postdata();
        
        echo '</main>';

    get_footer();