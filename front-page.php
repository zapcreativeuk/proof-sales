<?php
/**
 * The template for displaying the front page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Proof Sales Demo
 */


    get_header();
        
    get_sidebar();

        echo '<main id="main">';
        
            while( have_posts() ) :
                the_post();    
                the_content();                    
            endwhile;
            wp_reset_postdata();
        
        echo '</main>';

    get_footer();