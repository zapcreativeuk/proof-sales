<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Proof Sales Demo
 */

    get_header();

        $classes = get_body_class();
        ( ! in_array( 'woocommerce-checkout', $classes ) ? get_sidebar() : '' );

        echo '<main id="main" class="order-1 order-md-2">';
        
            while( have_posts() ) :
                the_post();    
                the_content();                    
            endwhile;
            wp_reset_postdata();
        
        echo '</main>';

    get_footer();