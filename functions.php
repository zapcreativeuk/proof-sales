<?php

    function proofDrinks_resources() {
        // Scripts
        wp_enqueue_script( 'proofDrinks_dist_vendors', get_template_directory_uri() . '/dist/js/vendors.bundle.js', array(), false, true  );
        wp_enqueue_script( 'proofDrinks_dist_scripts', get_template_directory_uri() . '/dist/js/scripts.bundle.js', array(), false, true  );
        wp_enqueue_script( 'proofDrinks_dist_main', get_template_directory_uri() . '/dist/js/main.bundle.js', array(), false, true  );
        // Stylesheets
        wp_enqueue_style( 'proofDrinks_fontawesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', NULL, microtime() );
        wp_enqueue_style( 'proofDrinks_bootstrap_styles', get_template_directory_uri() . '/dist/css/bootstrap.css', NULL, microtime() );
        wp_enqueue_style( 'proofDrinks_custo_login', get_stylesheet_directory_uri() . '/dist/css/login.css' );
        wp_enqueue_style( 'proofDrinks_main_styles', get_template_directory_uri() . '/dist/css/main.css', NULL, microtime() );
        wp_enqueue_style( 'proofDrinks_styles', get_stylesheet_uri(), NULL, microtime() );
    }

    add_action( 'wp_enqueue_scripts', 'proofDrinks_resources' );

    /**
     * Setup theme defaults and register support for various WordPress features
     */

    function proof_drinks_woocommerce_support() {
        add_theme_support( 'woocommerce', array(
            'thumbnail_image_width' => 150,
            'single_image_width'    => 300,
    
            'product_grid'          => array(
                'default_rows'    => 3,
                'min_rows'        => 2,
                'max_rows'        => 8,
                'default_columns' => 4,
                'min_columns'     => 2,
                'max_columns'     => 5,
            ),
        ) );
    }
    
    add_action( 'after_setup_theme', 'proof_drinks_woocommerce_support' );

    /**
     * Register Widgets
     */

    function proofdrinks_widgets() {

        register_sidebar( array(
            'name'          => 'Index Sidebar',
            'id'            => 'proof_sidebar_menu',
            'before_title'  => '<h6 class="d-none text-uppercase mb-1 font-weight-bold">',
            'after_title'   => '</h6>',
            'before_widget' => '<div class="sidebar-cat" style="">',
            'after_widget'  => '</div>'
        ) );
    
    }
    add_action( 'widgets_init', 'proofdrinks_widgets' );

    /**
     * User Login
     */
    // function redirect_dashboard() {  
    //     if (!current_user_can('administrator') && is_admin() && !wp_doing_ajax()) {  
    //         wp_redirect( home_url('/login') );  
    //         exit;  
    //     }
    // }
    // add_action('admin_init', 'redirect_dashboard');

    // function login_failed() {  
    //     $login_page  = home_url( '/login' );  
    //     wp_redirect( $login_page . '?login=failed' );  
    //     exit;  
    // }
    
    // add_action( 'wp_login_failed', 'login_failed' );  
    // function verify_username_password( $user, $username, $password ) {  
    //     $login_page  = home_url( '/login' );  
    //     if( $username == "" || $password == "" ) {  
    //         wp_redirect( $login_page . "?login=empty" );  
    //         exit;  
    //     }
    // }

    // if (!is_user_logged_in()) {  
    //     wp_redirect(home_url('/login'));  
    //     exit;  
    // }

    /**
     * Override loop template and show quantities next to add to cart buttons
     */
    add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );
    function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {
        if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
            $html = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
            $html .= woocommerce_quantity_input( array(), $product, false );
            $html .= '<button type="submit" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
            $html .= '</form>';
        }
        return $html;
    }

    add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
    
    // To change add to cart text on single product page
    add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
    function woocommerce_custom_single_add_to_cart_text() {
        return __( 'Add To Order', 'woocommerce' ); 
    }

    // To change add to cart text on product archives(Collection) page
    add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );  
    function woocommerce_custom_product_add_to_cart_text() {
        return __( 'Add to order', 'woocommerce' );
    }

    function woocommerce_button_proceed_to_checkout() {
        $checkout_url = WC()->cart->get_checkout_url();
        ?>
        <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward">
            <?php _e( 'Complete Order', 'woocommerce' ); ?>
        </a>
        <?php
    }

    // Added to order
    function woo_custom_change_cart_string($translated_text, $text, $domain) {
        $translated_text = str_replace("view cart", "View Order", $translated_text);
    return $translated_text;
    }
    add_filter('gettext', 'woo_custom_change_cart_string', 100, 3);
    add_filter('ngettext', 'woo_custom_change_cart_string', 100, 3);

    /**
     * Basket
     */

    add_filter( 'gettext', function( $translated_text ) {
    if ( 'View cart' === $translated_text ) {
        $translated_text = 'View Order';
    }
    return $translated_text;
    } );
    
    function custom_empty_cart_message() {
        $html  = '<div class="alert alert-warning text-center" role="alert"><p class="cart-empty">';
        $html .= wp_kses_post( apply_filters( 'wc_empty_cart_message', __( 'There are no items in your order', 'woocommerce' ) ) );
        echo $html . '</div></p>';
    }

    remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
    add_action( 'woocommerce_cart_is_empty', 'custom_empty_cart_message', 10 );
    // Update Basket Button
    function change_update_cart_text( $translated, $text, $domain ) {
        if( is_cart() && $translated == 'Update basket' ){
            $translated = 'Update Order';
        }
        return $translated;
    }
    add_filter( 'gettext', 'change_update_cart_text', 20, 3 );

    // Basket Updated Message
    function wpse_124400_woomessages($translation, $text, $domain) {
        if ($domain == 'woocommerce') {
            if ($text == 'Cart updated.') {
                $translation = 'Order Updated.';
            }
        }
        return $translation;
    }
    add_filter('gettext', 'wpse_124400_woomessages', 10, 3);

    /** 
     * Checkout
    */

    // Change the 'Billing details' checkout label to 'Contact Information'
    function wc_billing_field_strings( $translated_text, $text, $domain ) {
        switch ( $translated_text ) {
            case 'Billing details' :
        $translated_text = __( 'Delivery Address', 'woocommerce' );
            break;
        }
        return $translated_text;
    }
    add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

    // Remove shipping fields
    function wc_remove_checkout_fields( $fields ) {

        // Billing fields
        unset( $fields['billing']['billing_email'] );
        unset( $fields['billing']['billing_phone'] );
        unset( $fields['billing']['billing_country'] );

        // Shipping fields

        // Order fields

        return $fields;
    }
    add_filter( 'woocommerce_checkout_fields', 'wc_remove_checkout_fields' );

    // Change shipping field names
    add_filter('woocommerce_checkout_fields', function($fields) {
        $fields['billing']['billing_company']['label'] = __('Venue Name', 'textdomain');
        return $fields;
    });

    function elex_remove_checkout_optional_text( $field, $key, $args, $value ) {
        if( is_checkout() && ! is_wc_endpoint_url() ) {
            $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
            $field = str_replace( $optional, '', $field );
        }
        return $field;
    }
    add_filter( 'woocommerce_form_field' , 'elex_remove_checkout_optional_text', 10, 4 );

    // Add Rep Name
    function add_rep_name_billing_address () {
        $current_user = wp_get_current_user();
        echo '
            <div class="woocommerce-billing-fields__field-wrapper">
                <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70">
                    <label for="billing_city" class="">Order By</label>
                    <span class="woocommerce-input-wrapper" style="font-size: 1.5rem; color: #d17f48;">' . $current_user->display_name . '</span>
                </p>
            </div>
        ';
    }
    add_action( 'woocommerce_before_checkout_billing_form', 'add_rep_name_billing_address', 10, 1 );

    function checkout_no_autofil( $value, $input ) {
        $item_to_set_null = array(
                'billing_first_name',
                'billing_last_name',
                'billing_company',
                'billing_address_1',
                'billing_address_2',
                'billing_city',
                'billing_postcode',
                'billing_country',
                'billing_state',
                'billing_email',
                'billing_phone',
            ); // All the fields in this array will be set as empty string, add or remove as required.

        if (in_array($input, $item_to_set_null)) {
            $value = '';
        }
        return $value;
    }
    add_filter( 'woocommerce_checkout_get_value', 'checkout_no_autofil', 10, 2 );

    // Add custom shipping fields
    function add_delivery_date_billing_address () {
        $date = date("Y-m-j");
        echo '
            <div class="woocommerce-billing-fields__field-wrapper">
                <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70">
                    <label for="billing_city" class="">Preferred Delivery Date&nbsp;<abbr class="required" title="required">*</abbr></label>
                    <span class="woocommerce-input-wrapper">
                        <input type="date" id="date" name="order_delivery_date" value="' . date("Y-m-j", strtotime($Date. ' + 2 days')) . '" min="' . date("Y-m-j", strtotime($Date. ' + 2 days')) . '" max="2099-12-31">
                    </span>
                </p>
            </div>
        ';
    }
    add_action( 'woocommerce_after_checkout_billing_form', 'add_delivery_date_billing_address' , 10, 1 );
    
    function add_delivery_time_billing_address () {
        echo '
            <div class="woocommerce-billing-fields__field-wrapper">
                <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70">
                    <label for="billing_city" class="">Preferred Delivery Time&nbsp;<abbr class="required" title="required">*</abbr></label>
                    <span class="woocommerce-input-wrapper">
                        <select name="order_delivery_time" id="time">
                            <option value="">Please select</option>
                            <option value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>
                    </span>
                </p>
            </div>
        ';
    }
    add_action( 'woocommerce_after_checkout_billing_form', 'add_delivery_time_billing_address' , 10, 1 );

    // Add custom shipping fields to database
    function add_order_delivery_date_to_order ( $order_id ) {   
        if ( isset( $_POST ['order_delivery_date'] ) &&  '' != $_POST ['order_delivery_date'] ) {
            add_post_meta( $order_id, '_delivery_date',  sanitize_text_field( $_POST ['order_delivery_date'] ) );
        }
    }
    add_action( 'woocommerce_checkout_update_order_meta', 'add_order_delivery_date_to_order' , 10, 1);

    function add_order_delivery_time_to_order ( $order_id ) {   
        if ( isset( $_POST ['order_delivery_time'] ) &&  '' != $_POST ['order_delivery_time'] ) {
            add_post_meta( $order_id, '_delivery_time',  sanitize_text_field( $_POST ['order_delivery_time'] ) );
        }
    }
    add_action( 'woocommerce_checkout_update_order_meta', 'add_order_delivery_time_to_order' , 10, 1);

    // Add custom shipping fields to emails
    function add_delivery_date_to_emails ( $fields, $sent_to_admin, $order ) {
        if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
            $order_id = $order->get_id();
        } else {
            $order_id = $order->id;
        }

        $delivery_date = get_post_meta( $order_id, '_delivery_date', true );

        if ( '' != $delivery_date ) {
        $fields[ 'Delivery Date' ] = array(
            'label' => __( 'Delivery Date', 'add_extra_fields' ),
            'value' => $delivery_date,
        );
        }
        return $fields;
    }
    add_filter( 'woocommerce_email_order_meta_fields', 'add_delivery_date_to_emails' , 10, 3 );
    
    function add_delivery_time_to_emails ( $fields, $sent_to_admin, $order ) {
        if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
            $order_id = $order->get_id();
        } else {
            $order_id = $order->id;
        }

        $delivery_time = get_post_meta( $order_id, '_delivery_time', true );

        if ( '' != $delivery_time ) {
        $fields[ 'Delivery Time' ] = array(
            'label' => __( 'Delivery Time', 'add_extra_fields' ),
            'value' => $delivery_time,
        );
        }
        return $fields;
    }
    add_filter( 'woocommerce_email_order_meta_fields', 'add_delivery_time_to_emails' , 10, 3 );

    // Add custom fields to order confirmation
    function add_delivery_date_to_order_received_page ( $order ) {
        if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
            $order_id = $order->get_id();
        } else {
            $order_id = $order->id;
        }
        
        $delivery_date = get_post_meta( $order_id, '_delivery_date', true );
        
        if ( '' != $delivery_date ) {
            echo '<div class="woocommerce-order-overview__order order">' . __( 'Delivery Date', 'add_extra_fields' ) . ': <strong>' . $delivery_date . '</strong></div>';
        }
    }
    add_filter( 'woocommerce_order_details_after_order_table', 'add_delivery_date_to_order_received_page', 10 , 1 );
    function add_delivery_time_to_order_received_page ( $order ) {
        if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
            $order_id = $order->get_id();
        } else {
            $order_id = $order->id;
        }
        
        $delivery_time = get_post_meta( $order_id, '_delivery_time', true );
        
        if ( '' != $delivery_time ) {
            echo '<div class="woocommerce-order-overview__order order">' . __( 'Delivery Time', 'add_extra_fields' ) . ': <strong>' . $delivery_time . '</strong></div>';
        }
    }
    add_filter( 'woocommerce_order_details_after_order_table', 'add_delivery_time_to_order_received_page', 10 , 1 );

    function add_delivery_date_to_admin_order_page($order){
        echo '<p><strong>'.__('Delivery Date').':</strong> <br/>' . get_post_meta( $order->get_id(), '_delivery_date', true ) . '</p>';
    }
    add_action( 'woocommerce_admin_order_data_after_billing_address', 'add_delivery_date_to_admin_order_page', 10, 1 );
    function add_delivery_time_to_admin_order_page($order){
        echo '<p><strong>'.__('Delivery Time').':</strong> <br/>' . get_post_meta( $order->get_id(), '_delivery_time', true ) . '</p>';
    }
    add_action( 'woocommerce_admin_order_data_after_billing_address', 'add_delivery_time_to_admin_order_page', 10, 1 );

    // Rename order status
    function wc_renaming_order_status( $order_statuses ) {
        foreach ( $order_statuses as $key => $status ) {
            if ( 'wc-completed' === $key ) 
            $order_statuses['wc-completed'] = _x( 'Order Received', 'Order status', 'woocommerce' );
        }
        return $order_statuses;
    }
    add_filter( 'wc_order_statuses', 'wc_renaming_order_status' );

    // Auto checked delivery option
    add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true');

    // Auto On hold all WooCommerce orders.
    function custom_woocommerce_auto_complete_order( $order_id ) {
        if ( ! $order_id ) {
            return;
        }

        $order = wc_get_order( $order_id );
        if( 'processing'== $order->get_status() ) {
            $order->update_status( 'on-hold' );
        }
    }
    add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );

    // Order Again Button
    // function cs_add_order_again_to_my_orders_actions( $actions, $order ) {
    //     if ( $order->has_status( 'completed' ) ) {
    //         $actions['order-again'] = array(
    //             'url'  => wp_nonce_url( add_query_arg( 'order_again', $order->id ) , 'woocommerce-order_again' ),
    //             'name' => __( 'Order Again', 'woocommerce' )
    //         );
    //     }

    //     return $actions;
    // }

    // add_filter( 'woocommerce_my_account_my_orders_actions', 'cs_add_order_again_to_my_orders_actions', 50, 2 );

    /**
     * Emails
     */

    
    // Remove the shipping row from woocommerce_get_order_item_totals array 
    function remove_shipping_details_from_emails($total_rows, $obj, $tax_display)
    {
        if ( isset($total_rows['shipping']) )
        {
            unset($total_rows['shipping']);
        }

        return $total_rows;
    }

    add_filter( 'woocommerce_get_order_item_totals', 'remove_shipping_details_from_emails', 10, 3 );

    function remove_paymeny_method_from_emails( $total_rows, $order, $tax_display ){
        // On Email notifications only
        if ( ! is_wc_endpoint_url() ) {
            unset($total_rows['payment_method']);
        }
        return $total_rows;
    }

    add_filter( 'woocommerce_get_order_item_totals', 'remove_paymeny_method_from_emails', 10, 3 );

    // Force login password exemption
    function my_forcelogin_bypass( $bypass, $visited_url ) {
        if ( is_page('lost-password') ) {
            $bypass = true;
          }
        $allowed = array(
            home_url( '/my-account/lost-password/' ),
            wp_lostpassword_url()
        );
        if ( ! $bypass ) {
            $bypass = in_array( $visited_url, $allowed );
        }
    
        return $bypass;
    }
    add_filter( 'v_forcelogin_bypass', 'my_forcelogin_bypass', 10, 2 );